FROM jfrog-docker-registry.bintray.io/jfrog/artifactory-registry:latest
MAINTAINER Scandio GmbH

# Environment variables from Artifactory image
ENV ARTIFACTORY_HOME		/var/opt/jfrog/artifactory

# Environment variables for container initialization
ENV INIT_DIRECTORY			/docker/artifactory/init
ENV TEMP_DIRECTORY			/docker/artifactory/temp

# Create init and temp directory
RUN mkdir -p ${INIT_DIRECTORY} && mkdir -p ${TEMP_DIRECTORY}

# Copy start script and set correct permissions
COPY init/* ${INIT_DIRECTORY}/
RUN chmod 740 ${INIT_DIRECTORY}/*.sh

# Install necessary packages
RUN yum install -y \
		openssh-clients \
# Package 'rsync' is already installed
	&& yum clean all

# Execute start script
CMD ${INIT_DIRECTORY}/configure-and-start.sh