#!/bin/bash

########################################################################
# Global variables
########################################################################

script_name=${0##*/}
skip_file="SKIP_BACKUP_RESTORE"

########################################################################
# Arguments
########################################################################

backup_source=""
backup_target=""
backup_identity_file="~/.ssh/id_rsa"

########################################################################
# Functions
########################################################################

function show_usage {
	echo "Usage: ${script_name} -s <SOURCE> -t <TARGET> [-i <IDENTITY_FILE>]"
	echo
	echo "Copies the <SOURCE> to <TARGET> using 'rsync' and 'ssh' with the given <IDENTITY_FILE>."
	echo
	echo "Options:"
	echo "  -s <SOURCE>			Rsync URI to the remote host."
	echo "  -t <TARGET>			Path to local folder."
	echo "  -i <IDENTITY_FILE>		Path to identity file. If not set, the default '~/.ssh/id_rsa' will be used."
}

########################################################################
# Parse arguments
########################################################################

while getopts "hs:t:i:" option; do
	case ${option} in
		h)
			show_usage
			exit 0
			;;
		s)
			backup_source=${OPTARG}
			;;
		t)
			backup_target=$(readlink -f ${OPTARG})
			;;
		i)
			backup_identity_file=$(readlink -f ${OPTARG})
			;;
		'?')
			show_usage
			exit 1
			;;
	esac
done

########################################################################
# Check arguments
########################################################################

[ -z ${backup_source} ] && echo "Error: Missing argument SOURCE" && show_usage && exit 1
[ -z ${backup_target} ] && echo "Error: Missing argument TARGET" && show_usage && exit 1
[ -z ${backup_identity_file} ] && echo "Error: Missing argument IDENTITY_FILE" && show_usage && exit 1

[ ! -d ${backup_target} ] && echo "Error: Argument '${backup_target}' is not a directory" && show_usage && exit 1
[ ! -w ${backup_target} ] && echo "Error: Current user does not have write permissions for '${backup_target}'" && show_usage && exit 1
[ ! -f ${backup_identity_file} ] && echo "Error: Argument '${backup_identity_file}' is not a file" && show_usage && exit 1
[ ! -r ${backup_identity_file} ] && echo "Error: Current user does not have read permissions for '${backup_identity_file}'" && show_usage && exit 1

########################################################################
# Main script
########################################################################

if [ ! -f ${backup_target}/${skip_file} ]; then
	echo "Copying files from '${backup_source}' to '${backup_target}'"

	rsync -arcz -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${backup_identity_file}" ${backup_source} ${backup_target}
	touch ${backup_target}/${skip_file}

	echo "Successfully copied files from '${backup_source}' to '${backup_target}'"
fi

exit 0