#!/bin/bash

########################################################################
# Global variables
########################################################################

script_name=${0##*/}
skip_file="SKIP_CERTIFICATE_IMPORT"
keytool=$(readlink -f /usr/bin/java | sed "s|java$|keytool|")
jre_keystore=$(readlink -f /usr/bin/java | sed "s|bin/java$|lib/security/cacerts|")
default_password="changeit"

########################################################################
# Arguments
########################################################################

import_directory=""
keystore=${jre_keystore}
keystore_password=${default_password}

########################################################################
# Functions
########################################################################

function show_usage {
	echo "Usage: ${script_name} -d <DIRECTORY> [-k <KEYSTORE>] [-p <PASSWORD>]"
	echo
	echo "Imports all fs found in <DIRECTORY> into the Java keystore <KEYSTORE> with the given <PASSWORD>."
	echo
	echo "Options:"
	echo "  -d <DIRECTORY>		Path to directory, that contains the certificates to import"
	echo "  -k <KEYSTORE>		Path to Java keystore. If not set, the JRE keystore will be used."
	echo "  -p <PASSWORD>		Password of the keystore. If not set, the default 'changeit' wil be used."
}

function import_certificates {
	import_directory=$1
	keystore=$2
	keystore_password=$3

	echo "Importing certificates from directory '${import_directory}' to keystore '${keystore}'"

	for f in ${import_directory}/*; do
		echo "Importing file ${f}"
		${keytool} -keystore ${keystore} -storepass ${keystore_password} -importcert -alias ${f} -file ${f} -noprompt
	done

	echo "Successfully imported certificates from directory '${import_directory}' to keystore '${keystore}'"
}

function set_catalina_truststore {
	truststore=$1
	truststore_password=$2

	CATALINA_OPTS="$CATALINA_OPTS -Djavax.net.ssl.truststore=${truststore}"
	CATALINA_OPTS="$CATALINA_OPTS -Djavax.net.ssl.truststorePassword=${truststore_password}"
}

########################################################################
# Parse arguments
########################################################################

while getopts "hd:k:p:" option; do
	case ${option} in
		h)
			show_usage
			exit 0
			;;
		d)
			import_directory=$(readlink -f ${OPTARG})
			;;
		k)
			keystore=$(readlink -f ${OPTARG})
			;;
		p)
			keystore_password=${OPTARG}
			;;
		'?')
			show_usage
			exit 1
			;;
	esac
done

########################################################################
# Check arguments
########################################################################

[ -z ${import_directory} ] && echo "Error: Missing argument DIRECTORY" && show_usage && exit 1
[ -z ${keystore} ] && echo "Error: Missing argument KEYSTORE" && show_usage && exit 1
[ -z ${keystore_password} ] && echo "Error: Missing argument PASSWORD" && show_usage && exit 1

[ ! -d ${import_directory} ] && echo "Error: Argument '${import_directory}' is not a directory" && show_usage && exit 1
[ ! -r ${import_directory} ] && echo "Error: Current user does not have read permissions for '${import_directory}'" && show_usage && exit 1
[ ! -f ${keystore} ] && echo "Error: Argument '${keystore}' is not a file" && show_usage && exit 1

########################################################################
# Main script
########################################################################

if [ ! -f ${TEMP_DIRECTORY}/${skip_file} ]; then
	import_certificates ${import_directory} ${keystore} ${keystore_password}
	set_catalina_truststore ${keystore} ${keystore_password}
	touch ${TEMP_DIRECTORY}/${skip_file}
fi

exit 0